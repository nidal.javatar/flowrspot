package com.nidal.flowrspot.flowrspot;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HttpHeaders;
import com.nidal.flowrspot.flowrspot.model.user.dto.LoginRequest;
import com.nidal.flowrspot.flowrspot.model.user.dto.SignUpRequest;
import com.nidal.flowrspot.flowrspot.security.JwtAuthenticationResponse;
import lombok.Getter;
import lombok.SneakyThrows;
import com.google.common.base.Charsets;
import lombok.Value;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class IntegrationTestConfig {

    @Autowired
    @Getter
    protected MockMvc template;

    @Getter
    private ObjectMapper mapper = new ObjectMapper();


    public IntegrationTestConfig() {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @SneakyThrows
    protected <T> T object(Class<T> clazz, ResultActions resultActions) {
        resultActions.andExpect(status().is2xxSuccessful());
        String response = resultActions.andReturn().getResponse().getContentAsString();
        return mapper.readerFor(clazz).readValue(response);
    }

    @SneakyThrows
    protected <T> String json(T object) {
        mapper.findAndRegisterModules();
        return mapper.writeValueAsString(object);
    }

    @SneakyThrows
    private String token(Login login) {

        template.perform(post("/api/auth/signup")
                .with(postRequest(new SignUpRequest(login.username, login.username + "@test.com", login.password), null)))
                .andExpect(status().isOk());

        return object(JwtAuthenticationResponse.class, getTemplate().perform(post("/api/auth/login")
                .with(postRequest(new LoginRequest(login.username, login.getPassword()), null)))
                .andExpect(status().isOk())).getAccessToken();

    }

    protected <T> RequestPostProcessor postRequest(T request, Login user) {
        String content = json(request);
        return mockRequest -> {
            if (user != null) {
                mockRequest.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token(user));
            }
            mockRequest.setContent(content.getBytes(Charsets.UTF_8));
            mockRequest.setContentType(MediaType.APPLICATION_JSON_UTF8.toString());
            return mockRequest;
        };
    }

    protected RequestPostProcessor getRequest(Login user) {
        return mockRequest -> {
            if (user != null) {
                mockRequest.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token(user));
            }
            mockRequest.setContentType(MediaType.APPLICATION_JSON_UTF8.toString());
            return mockRequest;
        };
    }

    @Value
    protected static class Login {
        String username;
        String password;

        public static final Login USER = new Login("test", "testx");
        public static final Login ADMIN = new Login("admin", "admin");
    }

}
