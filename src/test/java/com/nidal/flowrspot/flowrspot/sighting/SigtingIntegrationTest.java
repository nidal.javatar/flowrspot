package com.nidal.flowrspot.flowrspot.sighting;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nidal.flowrspot.flowrspot.FlowrspotApplication;
import com.nidal.flowrspot.flowrspot.IntegrationTestConfig;
import com.nidal.flowrspot.flowrspot.model.flower.dto.FlowerResponse;
import com.nidal.flowrspot.flowrspot.model.sighting.dto.SightingRequest;
import com.nidal.flowrspot.flowrspot.model.sighting.dto.SightingResponse;
import com.nidal.flowrspot.flowrspot.model.user.dao.User;
import com.nidal.flowrspot.flowrspot.model.user.dto.LoginRequest;
import com.nidal.flowrspot.flowrspot.model.user.dto.SignUpRequest;
import com.nidal.flowrspot.flowrspot.model.user.dto.UserResponse;
import com.nidal.flowrspot.flowrspot.security.JwtAuthenticationResponse;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlowrspotApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class SigtingIntegrationTest extends IntegrationTestConfig {

    @Test
    public void testLoggedPublicSigting() throws Exception {
        getTemplate().perform(get("/api/flower/public/top")
                .with(getRequest(Login.USER)))
                .andExpect(status().isOk());
    }

    @Test
    public void testNotLoggedPublicSighting() throws Exception {
        String jsonResponse = getTemplate().perform(get("/api/flower/public/top")
                .with(getRequest(null)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        String flowerResponse = jsonResponse.substring(jsonResponse.indexOf('{'), jsonResponse.indexOf('}') + 1);
        FlowerResponse response = getMapper().readerFor(FlowerResponse.class).readValue(flowerResponse);

        getTemplate().perform(get("/api/sighting/public/flower/" + response.getExternalId())
                .with(getRequest(null)))
                .andExpect(status().isOk());
    }


    @Test
    public void testLoggedPublicSighting() throws Exception {
        String jsonResponse = getTemplate().perform(get("/api/flower/public/top")
                .with(getRequest(null)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        String flowerResponse = jsonResponse.substring(jsonResponse.indexOf('{'), jsonResponse.indexOf('}') + 1);
        FlowerResponse response = getMapper().readerFor(FlowerResponse.class).readValue(flowerResponse);

        getTemplate().perform(get("/api/sighting/public/flower/" + response.getExternalId())
                .with(getRequest(Login.ADMIN)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateSighting() throws Exception {
        String jsonResponse = getTemplate().perform(get("/api/flower/public/top")
                .with(getRequest(null)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        String flowerResponse = jsonResponse.substring(jsonResponse.indexOf('{'), jsonResponse.indexOf('}') + 1);
        FlowerResponse response = getMapper().readerFor(FlowerResponse.class).readValue(flowerResponse);

        SightingRequest request = new SightingRequest();
        request.setFlowerExternalId(response.getExternalId());
        request.setLatitude("111.222");
        request.setLongitude("111.222");
        request.setImageContent("");

        SightingResponse sightingResponse = object(SightingResponse.class,
                getTemplate().perform(post("/api/sighting/")
                        .with(postRequest(request,new Login("user24", "passwo343"))))
                        .andExpect(status().isOk()));

        assertThat(sightingResponse.getExternalId()).isNotNull();
        assertThat(sightingResponse.getLatitude()).isEqualTo(request.getLatitude());
        assertThat(sightingResponse.getLongitude()).isEqualTo(request.getLongitude());

    }

    @Test
    public void testLikeSighting() throws Exception {
        String jsonResponse = getTemplate().perform(get("/api/flower/public/top")
                .with(getRequest(null)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        String flowerResponse = jsonResponse.substring(jsonResponse.indexOf('{'), jsonResponse.indexOf('}') + 1);
        FlowerResponse response = getMapper().readerFor(FlowerResponse.class).readValue(flowerResponse);

        SightingRequest request = new SightingRequest();
        request.setFlowerExternalId(response.getExternalId());
        request.setLatitude("1113.222");
        request.setLongitude("1113.222");
        request.setImageContent("");

        SightingResponse sightingResponse = object(SightingResponse.class,
                getTemplate().perform(post("/api/sighting/")
                        .with(postRequest(request,new Login("user234e", "passwo3433e"))))
                        .andExpect(status().isOk()));

        Integer actual = object(Integer.class,
                getTemplate().perform(post(String.format("/api/sighting/%s/like", sightingResponse.getExternalId()))
                        .with(getRequest(new Login("user234", "passwo3433"))))
                        .andExpect(status().isOk()));

        assertThat(actual.intValue()).isEqualTo(1);

    }

    @Test
    public void testDisLikeSighting() throws Exception {
        String jsonResponse = getTemplate().perform(get("/api/flower/public/top")
                .with(getRequest(null)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        String flowerResponse = jsonResponse.substring(jsonResponse.indexOf('{'), jsonResponse.indexOf('}') + 1);
        FlowerResponse response = getMapper().readerFor(FlowerResponse.class).readValue(flowerResponse);

        SightingRequest request = new SightingRequest();
        request.setFlowerExternalId(response.getExternalId());
        request.setLatitude("11135.222");
        request.setLongitude("11135.222");
        request.setImageContent("");

        SightingResponse sightingResponse = object(SightingResponse.class,
                getTemplate().perform(post("/api/sighting/")
                        .with(postRequest(request,new Login("user4e", "passwo33e"))))
                        .andExpect(status().isOk()));

        object(Integer.class,
                getTemplate().perform(post(String.format("/api/sighting/%s/like", sightingResponse.getExternalId()))
                        .with(getRequest(new Login("userkio234", "password5333"))))
                        .andExpect(status().isOk()));

        Integer actual = object(Integer.class,
                getTemplate().perform(delete(String.format("/api/sighting/%s/dislike", sightingResponse.getExternalId()))
                        .with(getRequest(new Login("usox234", "pass33"))))
                        .andExpect(status().isOk()));

        assertThat(actual.intValue()).isEqualTo(0);

    }

}
