package com.nidal.flowrspot.flowrspot.flower;

import com.nidal.flowrspot.flowrspot.FlowrspotApplication;
import com.nidal.flowrspot.flowrspot.IntegrationTestConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlowrspotApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class FlowerIntegrationTest extends IntegrationTestConfig {

    @Test
    public void testLoggedPublicFlower() throws Exception {
        getTemplate().perform(get("/api/flower/public/top")
                .with(getRequest(new Login("wewewe", "weewew"))))
                .andExpect(status().isOk());
    }

    @Test
    public void testNotLoggedPublicFlower() throws Exception {
        getTemplate().perform(get("/api/flower/public/top")
                .with(getRequest(null)))
                .andExpect(status().isOk());
    }



}
