package com.nidal.flowrspot.flowrspot.auth;

import com.nidal.flowrspot.flowrspot.FlowrspotApplication;
import com.nidal.flowrspot.flowrspot.IntegrationTestConfig;
import com.nidal.flowrspot.flowrspot.model.user.dto.LoginRequest;
import com.nidal.flowrspot.flowrspot.model.user.dto.SignUpRequest;
import com.nidal.flowrspot.flowrspot.security.JwtAuthenticationResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlowrspotApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class AuthenticationRestControllerTest extends IntegrationTestConfig {

    @Test
    public void testSignUpAndLogin() throws Exception {
        getTemplate().perform(post("/api/auth/signup")
                .with(postRequest(new SignUpRequest("test12345", "test123@test.com", "test123"), null)))
            .andExpect(status().isOk());

        String token = object(JwtAuthenticationResponse.class, getTemplate().perform(post("/api/auth/login")
                .with(postRequest(new LoginRequest("test12345", "test123"), null)))
            .andExpect(status().isOk())).getAccessToken();

        assertThat(token).isNotNull();
    }

    @Test
    public void testAnonymousLogin() throws Exception {
       getTemplate().perform(post("/api/auth/login")
                .with(postRequest(new LoginRequest("test322", "test123232"), null)))
                .andExpect(status().isUnauthorized());
    }
}
