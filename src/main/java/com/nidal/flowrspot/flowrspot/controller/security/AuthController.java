package com.nidal.flowrspot.flowrspot.controller.security;

import com.nidal.flowrspot.flowrspot.model.user.dto.LoginRequest;
import com.nidal.flowrspot.flowrspot.model.user.dto.SignUpRequest;
import com.nidal.flowrspot.flowrspot.model.user.dto.UserResponse;
import com.nidal.flowrspot.flowrspot.security.JwtAuthenticationResponse;
import com.nidal.flowrspot.flowrspot.security.JwtTokenProvider;
import com.nidal.flowrspot.flowrspot.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired(required = true)
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final JwtTokenProvider tokenProvider;

    private final UserService userService;

    public AuthController(JwtTokenProvider tokenProvider,
                          UserService userService) {
        this.tokenProvider = tokenProvider;
        this.userService = userService;
    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<UserResponse> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {

        if(userService.existsByUsername(signUpRequest.getUsername())) {
            throw new IllegalArgumentException("Username already in use!");
        }
        if(userService.existsByEmail(signUpRequest.getEmail())) {
            throw new IllegalArgumentException("Email address already in use!");
        }

        signUpRequest.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
        return ResponseEntity.ok(userService.save(signUpRequest));
    }
}
