package com.nidal.flowrspot.flowrspot.controller.sighting;

import com.nidal.flowrspot.flowrspot.model.sighting.dto.SightingRequest;
import com.nidal.flowrspot.flowrspot.model.sighting.dto.SightingResponse;
import com.nidal.flowrspot.flowrspot.service.flower.FlowerService;
import com.nidal.flowrspot.flowrspot.service.sighting.SightingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/sighting")
public class SightingController {

    private final SightingService sightingService;

    public SightingController(SightingService sightingService) {
        this.sightingService = sightingService;
    }

    @PostMapping("/")
    public ResponseEntity<SightingResponse> createSighting(@RequestBody @Valid SightingRequest request) {

        return ResponseEntity.ok(sightingService.save(request));
    }

    @GetMapping("/public/flower/{externalId}")
    public ResponseEntity<List<SightingResponse>> getPublicSighting(@PathVariable UUID externalId) {
        return ResponseEntity.ok(sightingService.getFlowerSighting(externalId));
    }

    @PostMapping("/{externalId}/like")
    public ResponseEntity<Integer> giveLike(@PathVariable UUID externalId){
        return ResponseEntity.ok(sightingService.giveLike(externalId));
    }

    @DeleteMapping("/{externalId}")
    public ResponseEntity<SightingResponse> deleteSighting(@PathVariable UUID externalId) {
        return ResponseEntity.ok(sightingService.deleteSighting(externalId));
    }

    @DeleteMapping("/{externalId}/dislike")
    public ResponseEntity<Integer> removeLike(@PathVariable UUID externalId){
        return ResponseEntity.ok(sightingService.removeLike(externalId));
    }
}
