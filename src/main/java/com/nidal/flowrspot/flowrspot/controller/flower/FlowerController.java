package com.nidal.flowrspot.flowrspot.controller.flower;

import com.nidal.flowrspot.flowrspot.model.flower.dto.FlowerResponse;
import com.nidal.flowrspot.flowrspot.service.flower.FlowerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/flower")
public class FlowerController {

    private final FlowerService flowerService;

    public FlowerController(FlowerService flowerService) {
        this.flowerService = flowerService;
    }

    @GetMapping("/public/top")
    public ResponseEntity<List<FlowerResponse>> getPublicFlower() {

        return ResponseEntity.ok(flowerService.readPublic());
    }

}
