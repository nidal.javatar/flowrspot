package com.nidal.flowrspot.flowrspot.model.sighting.dao;

import com.nidal.flowrspot.flowrspot.common.model.Audit;
import com.nidal.flowrspot.flowrspot.model.flower.dao.Flower;
import com.nidal.flowrspot.flowrspot.model.like.dao.Like;
import com.nidal.flowrspot.flowrspot.model.user.dao.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true, exclude = {"likes"})
@ToString(callSuper=true, exclude = {"likes"})
@Entity
@Inheritance(strategy= InheritanceType.JOINED)
@Table(name = "sighting")
public class Sighting extends Audit {

    private static final long serialVersionUID = 624299371761026242L;

    @Column(name = "longitude", nullable = false)
    private String longitude;

    @Column(name = "latitude", nullable = false)
    private String latitude;

    @Column(name = "quote", nullable = false)
    private String quote;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private User user;

    @ManyToOne(optional = false)
    @JoinColumn(name = "flower_id", referencedColumnName = "id", nullable = false)
    private Flower flower;

    @Column(name = "image", nullable = false)
    private String image;

    @OneToMany(mappedBy = "pk.sighting", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Like> likes;
}
