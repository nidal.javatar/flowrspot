package com.nidal.flowrspot.flowrspot.model.like.dao;

import com.nidal.flowrspot.flowrspot.model.sighting.dao.Sighting;
import com.nidal.flowrspot.flowrspot.model.user.dao.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString(callSuper = true)
@Entity
@Table(name = "sighting_like",
        uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "sighting_id"})
)
public class Like implements Serializable {

    private static final long serialVersionUID = -4897708288838142559L;

    @EmbeddedId
    private LikePk pk;

    public Like(User user, Sighting sighting){
        this.pk = new LikePk(user, sighting);
    }
}
