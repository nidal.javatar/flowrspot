package com.nidal.flowrspot.flowrspot.model.flower.dao;

import com.nidal.flowrspot.flowrspot.common.model.Audit;
import com.nidal.flowrspot.flowrspot.model.sighting.dao.Sighting;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@Entity
@Inheritance(strategy= InheritanceType.JOINED)
@Table(name = "flower")
public class Flower extends Audit {

    private static final long serialVersionUID = 4802439541675411049L;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "image", nullable = false)
    private String image;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "flower", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Sighting> sightings;

}
