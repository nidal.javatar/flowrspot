package com.nidal.flowrspot.flowrspot.model.user.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@NoArgsConstructor
public class LoginRequest {

    private static final long serialVersionUID = -5344756486721480692L;

    private String usernameOrEmail;

    private String password;

    public LoginRequest(String usernameOrEmail, String password) {
        this.usernameOrEmail = usernameOrEmail;
        this.password = password;
    }
}
