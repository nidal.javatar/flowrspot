package com.nidal.flowrspot.flowrspot.model.flower.dto;

import lombok.Data;

import java.util.ArrayList;

public interface FlowerRest {

    @Data
    class Response {

        ArrayList<RestFlower> hits = new ArrayList<RestFlower>();

    }

    @Data
    class RestFlower {

        private String largeImageURL;

        private String tags;
    }
}
