package com.nidal.flowrspot.flowrspot.model.like.dao;

import com.nidal.flowrspot.flowrspot.model.sighting.dao.Sighting;
import com.nidal.flowrspot.flowrspot.model.user.dao.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@Embeddable
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"user", "sighting"})
@ToString(exclude = {"user", "sighting"})
class LikePk implements Serializable {

    private static final long serialVersionUID = -5198066082739716405L;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private User user;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "sighting_id", referencedColumnName = "id", nullable = false)
    private Sighting sighting;

    LikePk(User user, Sighting sighting) {
        this.user = user;
        this.sighting= sighting;
    }
}
