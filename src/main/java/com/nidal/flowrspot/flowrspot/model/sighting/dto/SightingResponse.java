package com.nidal.flowrspot.flowrspot.model.sighting.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class SightingResponse {

    private UUID externalId;

    private String longitude;

    private String latitude;

    private String quote;

    private String imageUrl;

    private Integer likes;
}
