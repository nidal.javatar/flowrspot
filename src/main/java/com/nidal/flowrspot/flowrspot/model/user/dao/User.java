package com.nidal.flowrspot.flowrspot.model.user.dao;

import com.nidal.flowrspot.flowrspot.common.model.Audit;
import com.nidal.flowrspot.flowrspot.model.like.dao.Like;
import com.nidal.flowrspot.flowrspot.model.sighting.dao.Sighting;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true, exclude = {"sightings", "likes"})
@ToString(callSuper=true, exclude = {"sightings", "likes"})
@Entity
@Inheritance(strategy= InheritanceType.JOINED)
@Table(name = "user_profile",
        uniqueConstraints = @UniqueConstraint(columnNames = "username")
)
public class User extends Audit {

    private static final long serialVersionUID = -7928961606709854408L;

    @Column(name = "email", nullable = false, unique = true)
    private @Pattern(regexp = ".+@.+\\..+", message = "Please provide a valid email address") String email;

    @Column(name = "username", nullable = false, unique = true)
    private @NotNull String username;

    @Column(name = "password", nullable = false)
    @NonNull
    private @Size(min = 5, message = "Password should be at least 5 characters long.") String password;

    @OneToMany(mappedBy = "user", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Sighting> sightings;

    @OneToMany(mappedBy = "pk.user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Like> likes;

    public User(String email, String username, String password){
        this.email = email;
        this.username = username;
        this.password = password;
    }

}
