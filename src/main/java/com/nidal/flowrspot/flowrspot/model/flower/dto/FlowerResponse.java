package com.nidal.flowrspot.flowrspot.model.flower.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class FlowerResponse {

    private UUID externalId;

    private String name;

    private String image;

    private String description;

}
