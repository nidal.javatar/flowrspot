package com.nidal.flowrspot.flowrspot.model.quote;

import lombok.Data;

@Data
public class QuoteDto {

        private String content;

        private String author;

}
