package com.nidal.flowrspot.flowrspot.model.user.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class UserResponse {

    private UUID externalId;

    private String email;

    private String username;

    public UserResponse(UUID externalId, String email, String username){
        this.email = email;
        this.username = username;
    }

}
