package com.nidal.flowrspot.flowrspot.model.user.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class SignUpRequest {

    private @NotBlank @Size(min = 3, max = 15) String username;

    private @NotBlank @Size(max = 40) @Email String email;

    private @NotBlank @Size(min = 5, max = 20) String password;


    public SignUpRequest(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }
}
