package com.nidal.flowrspot.flowrspot.model.sighting.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class SightingRequest {

    private UUID flowerExternalId;

    private String longitude;

    private String latitude;

    private String imageContent;

}
