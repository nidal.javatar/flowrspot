package com.nidal.flowrspot.flowrspot.common.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.Instant;

import static java.time.Instant.now;

@MappedSuperclass
@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
public abstract class Audit extends InternalEntity {

    @CreatedBy
    @Column(name = "created_by", unique = false, nullable = true, columnDefinition = "VARCHAR(255)")
    private String createdBy;

    @CreatedDate
    @Column(name = "created_on", unique = false, nullable = true, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Instant createdOn;

    @LastModifiedBy
    @Column(name = "modified_by", unique = false, nullable = true, columnDefinition = "VARCHAR(255)")
    private String modifiedBy;

    @LastModifiedDate
    @Column(name = "modified_on", unique = false, nullable = true, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Instant modifiedOn;

    @PrePersist
    public void persist() {
        this.createdOn = now();
        this.createdBy = getCurrentUser();
        this.modifiedOn = now();
    }

    @PreUpdate
    public void update() {
        this.modifiedOn = now();
        this.modifiedBy = getCurrentUser();
    }

    private String getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth != null ? auth.getName() : "UNKNOWN";
    }

}
