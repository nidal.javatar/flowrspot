package com.nidal.flowrspot.flowrspot.common.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.io.Serializable;
import java.util.UUID;


@MappedSuperclass
@Data
public abstract class InternalEntity implements Serializable {

    private static final long serialVersionUID = -9006398236340523793L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(name = "external_id", columnDefinition = "BINARY(16)", nullable = false, unique = true)
    private UUID externalId;

    @PrePersist
    public void autofill() {
        this.externalId = (externalId == null) ? UUID.randomUUID() : externalId;
    }


}
