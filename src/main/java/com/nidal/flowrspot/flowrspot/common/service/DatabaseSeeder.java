package com.nidal.flowrspot.flowrspot.common.service;

import com.nidal.flowrspot.flowrspot.api.flower.FlowerRepository;
import com.nidal.flowrspot.flowrspot.api.sighting.SightingRepository;
import com.nidal.flowrspot.flowrspot.model.flower.dao.Flower;
import com.nidal.flowrspot.flowrspot.model.sighting.dao.Sighting;
import com.nidal.flowrspot.flowrspot.service.webservice.WebRestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DatabaseSeeder {

    private static final Logger logger = LoggerFactory.getLogger(DatabaseSeeder.class);

    @Value("${app.seed.flower.count}")
    private int flowerSeedCount;
    private final FlowerRepository flowerRepository;
    private final SightingRepository sightingRepository;
    private final WebRestService restService;

    @Autowired
    public DatabaseSeeder(FlowerRepository flowerRepository,
                          SightingRepository sightingRepository,
                          WebRestService restService) {
        this.flowerRepository = flowerRepository;
        this.sightingRepository = sightingRepository;
        this.restService = restService;
    }

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        seedFlowerTable(flowerSeedCount);
        seedSightingTable();
    }

    private void seedFlowerTable(int seed) {
        List<Flower> flowers = flowerRepository.findAll();
        if(flowers.isEmpty()) {
            try {
               flowerRepository.saveAll(restService.getFlowersFromExternalApi(seed));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            logger.info("Flower table seeded");
        }else {
            logger.trace("Flower Seeding Not Required");
        }

    }

    private void seedSightingTable() {
        List<Sighting> sightings = sightingRepository.findAllByQuote("");
        if(!sightings.isEmpty()) {
            sightings.stream().map(sighting -> {
                sighting.setQuote(restService.getRandomQuoteFromExternalApi());
                sightingRepository.save(sighting);
                return true;
            }).collect(Collectors.toList());
            logger.info("Sighting table seeded");
        } else {
            logger.trace("Sighting Seeding Not Required");
        }
    }

}
