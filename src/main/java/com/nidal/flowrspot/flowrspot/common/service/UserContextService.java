package com.nidal.flowrspot.flowrspot.common.service;

import com.nidal.flowrspot.flowrspot.model.user.dao.User;
import com.nidal.flowrspot.flowrspot.security.UserPrincipal;
import com.nidal.flowrspot.flowrspot.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserContextService {

    private static final Logger logger = LoggerFactory.getLogger(DatabaseSeeder.class);

    private final UserService userService;

    public UserContextService(UserService userService) {
        this.userService = userService;
    }


    public UUID getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        logger.info(SecurityContextHolder.getContext().getAuthentication().getDetails().toString());
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userPrincipal.getExternalId();
    }

}
