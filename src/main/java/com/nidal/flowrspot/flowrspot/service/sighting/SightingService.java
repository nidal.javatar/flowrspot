package com.nidal.flowrspot.flowrspot.service.sighting;

import com.nidal.flowrspot.flowrspot.api.sighting.SightingRepository;
import com.nidal.flowrspot.flowrspot.common.service.UserContextService;
import com.nidal.flowrspot.flowrspot.model.flower.dao.Flower;
import com.nidal.flowrspot.flowrspot.model.like.dao.Like;
import com.nidal.flowrspot.flowrspot.model.sighting.dao.Sighting;
import com.nidal.flowrspot.flowrspot.model.sighting.dto.SightingRequest;
import com.nidal.flowrspot.flowrspot.model.sighting.dto.SightingResponse;
import com.nidal.flowrspot.flowrspot.model.user.dao.User;
import com.nidal.flowrspot.flowrspot.security.JwtTokenProvider;
import com.nidal.flowrspot.flowrspot.service.flower.FlowerService;
import com.nidal.flowrspot.flowrspot.service.user.UserService;
import com.nidal.flowrspot.flowrspot.service.webservice.WebRestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Base64;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SightingService {
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    private static final String IMAGE_PATH = "src/main/resources/static/images/";

    private final SightingRepository repository;
    private final WebRestService restService;
    private final FlowerService flowerService;
    private final UserService userService;
    private final UserContextService userContextService;

    public SightingService(SightingRepository repository,
                           WebRestService restService,
                           FlowerService flowerService,
                           UserService userService,
                           UserContextService userContextService) {
        this.repository = repository;
        this.restService = restService;
        this.flowerService = flowerService;
        this.userService = userService;
        this.userContextService = userContextService;
    }

    public SightingResponse save(SightingRequest entityRequest) {
        Flower flower = flowerService.findByExternalId(entityRequest.getFlowerExternalId())
                .orElseThrow(() -> new EntityNotFoundException(String.format("Flower not found %s",
                        entityRequest.getFlowerExternalId())));

        UUID userId = userContextService.getCurrentUser();
        User user = userService.findByExternalId(userId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("User not found %s", userId)));

        String imageName = String.format("%s%s%s", flower.getName(),UUID.randomUUID(), ".jpg");
        saveImage(entityRequest.getImageContent(), imageName);

        Sighting entity = mapSightingRequest(entityRequest);
        entity.setFlower(flower);
        entity.setUser(user);
        entity.setImage(String.format("%s%s", IMAGE_PATH, imageName));

        return mapSighting(repository.save(entity));
    }

    public List<SightingResponse> getFlowerSighting(UUID flowerId) {
        if(flowerService.existsByExternalId(flowerId)) {
            return repository.findAllByFlowerExternalId(flowerId).stream()
                    .map(this::mapSighting)
                    .sorted(Comparator.comparingInt(SightingResponse::getLikes).reversed())
                    .collect(Collectors.toList());
        } else {
            throw new EntityNotFoundException(String.format("Flower does not exist with: %s", flowerId));
        }
    }

    public Integer giveLike(UUID externalId){
        Sighting sighting = repository.findByExternalId(externalId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Sighting not found%s", externalId)));

        UUID userId = userContextService.getCurrentUser();
        User user = userService.findByExternalId(userId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("User not found%s", userId)));

        Like like = new Like(user, sighting);
        sighting.getLikes().add(like);
        repository.save(sighting);
        return sighting.getLikes().size();
    }

    public Integer removeLike(UUID externalId) {
        Sighting sighting = repository.findByExternalId(externalId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Sighting not found%s", externalId)));

        UUID userId = userContextService.getCurrentUser();
        User user = userService.findByExternalId(userId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("User not found%s", userId)));

        Like like = new Like(user, sighting);
        if (sighting.getLikes().contains(like)) {
            sighting.getLikes().remove(like);
            repository.save(sighting);
        }
        return sighting.getLikes().size();
    }

    public SightingResponse deleteSighting(UUID externalId){
        Sighting sighting = repository.findByExternalIdAndUserExternalId(externalId, userContextService.getCurrentUser());
        repository.delete(sighting);
        return mapSighting(sighting);
    }

    private Sighting mapSightingRequest(SightingRequest entityRequest) {
        Sighting entity = new Sighting();
        entity.setLatitude(entityRequest.getLatitude());
        entity.setLongitude(entityRequest.getLongitude());
        entity.setQuote(restService.getRandomQuoteFromExternalApi());
        entity.setLikes(new HashSet<>());

        return entity;
    }

    private SightingResponse mapSighting(Sighting entity) {
        SightingResponse response = new SightingResponse();
        response.setExternalId(entity.getExternalId());
        response.setLikes(entity.getLikes().size());
        response.setQuote(entity.getQuote());
        response.setLongitude(entity.getLongitude());
        response.setLatitude(entity.getLatitude());
        response.setImageUrl(entity.getImage());

        return response;
    }

    private void saveImage(String imageContent, String name) {
        byte[] decodedImg = Base64.getDecoder()
                .decode(imageContent.getBytes(StandardCharsets.UTF_8));
        try {
            Files.write(new File(String.format("%s%s", IMAGE_PATH, name)).toPath(), decodedImg);
            logger.info("Image saved");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
