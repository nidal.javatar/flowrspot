package com.nidal.flowrspot.flowrspot.service.webservice;

import com.nidal.flowrspot.flowrspot.common.service.DatabaseSeeder;
import com.nidal.flowrspot.flowrspot.model.flower.dao.Flower;
import com.nidal.flowrspot.flowrspot.model.flower.dto.FlowerRest;
import com.nidal.flowrspot.flowrspot.model.quote.QuoteDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class WebRestService {

    private static final Logger logger = LoggerFactory.getLogger(DatabaseSeeder.class);
    private static final String FLOWER_DATA = "https://pixabay.com/api/";
    //For given API "https://quotes.rest/" we need an API key to access this service,and api-key is not free.
    private static final String QUOTE_DATA = "https://api.quotable.io/random";

    private  RestTemplate restTemplate = new RestTemplate();

    public List<Flower> getFlowersFromExternalApi(int length) throws URISyntaxException {
        Map<String, String> params = new HashMap<String, String>();
        params.put("key", "13478039-768551f00ce055b4a932fe45e");
        params.put("q", "flowers");
        params.put("image_type", "photo");
        params.put("pretty", "true");
        params.put("per_page", String.format("%d", length));

        FlowerRest.Response result = restTemplate.getForObject(appendToUrl(FLOWER_DATA, params), FlowerRest.Response.class, params);

        assert result != null;
        return result.getHits().stream().map(hit -> {
            Flower flower = new Flower();
            flower.setDescription(hit.getTags());
            flower.setName(hit.getTags().substring(0, hit.getTags().indexOf(',')));
            flower.setImage(hit.getLargeImageURL());
            return flower;
        }).collect(Collectors.toList());

    }

    public String getRandomQuoteFromExternalApi(){
        QuoteDto result = restTemplate.getForObject(QUOTE_DATA, QuoteDto.class);
        assert result != null;
        return result.getContent();
    }

    public  String appendToUrl(String url, Map<String, String> parameters) throws URISyntaxException {
        URI uri = new URI(url);
        String query = uri.getQuery();

        StringBuilder builder = new StringBuilder();

        if (query != null) {
            builder.append(query);
        }

        for (Map.Entry<String, String> entry: parameters.entrySet()) {
            String keyValueParam = entry.getKey() + "=" + entry.getValue();
            if (!builder.toString().isEmpty()) {
                builder.append("&");
            }

            builder.append(keyValueParam);
        }

        URI newUri = new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), builder.toString(), uri.getFragment());
        return newUri.toString();
    }

}


