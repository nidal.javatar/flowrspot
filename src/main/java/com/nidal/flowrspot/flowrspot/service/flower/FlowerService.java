package com.nidal.flowrspot.flowrspot.service.flower;

import com.nidal.flowrspot.flowrspot.api.flower.FlowerRepository;
import com.nidal.flowrspot.flowrspot.model.flower.dao.Flower;
import com.nidal.flowrspot.flowrspot.model.flower.dto.FlowerResponse;
import com.nidal.flowrspot.flowrspot.model.sighting.dao.Sighting;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class FlowerService {

    private final FlowerRepository repository;

    public FlowerService(FlowerRepository repository) {
        this.repository = repository;
    }


    public List<FlowerResponse> readPublic() {
        return repository.findAllByOrderByCreatedOnAsc().stream()
                .map(flower -> {
                    FlowerResponse flowerResponse = new FlowerResponse();
                    flowerResponse.setExternalId(flower.getExternalId());
                    flowerResponse.setName(flower.getName());
                    flowerResponse.setImage(flower.getImage());
                    flowerResponse.setDescription(flower.getDescription());
                    return flowerResponse;
                }).collect(Collectors.toList());

    }

    public boolean existsByExternalId(UUID externalId) {
        return repository.existsByExternalId(externalId);
    }

    public Optional<Flower> findByExternalId(UUID flowerId) {
        return repository.findByExternalId(flowerId);
    }
}
