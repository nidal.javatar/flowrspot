package com.nidal.flowrspot.flowrspot.service.user;

import com.nidal.flowrspot.flowrspot.api.user.UserRepository;
import com.nidal.flowrspot.flowrspot.model.user.dao.User;
import com.nidal.flowrspot.flowrspot.model.user.dto.SignUpRequest;
import com.nidal.flowrspot.flowrspot.model.user.dto.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserResponse save(SignUpRequest userDto){
        User user = new User(userDto.getEmail(), userDto.getUsername(), userDto.getPassword());
        userRepository.save(user);

        return new UserResponse(user.getExternalId(),user.getEmail(), user.getUsername());
    }

    public Optional<User> findByExternalId(UUID externalId) {
        return userRepository.findByExternalId(externalId);
    }

    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

}
