package com.nidal.flowrspot.flowrspot.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nidal.flowrspot.flowrspot.model.user.dao.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
public class UserPrincipal implements UserDetails {

    private static final long serialVersionUID = -8295417334392282371L;

    private UUID externalId;

    private String username;

    @JsonIgnore
    private String email;

    @JsonIgnore
    private String password;

    private boolean enabled;
    private boolean locked;

    private Instant credentialsExpiry;
    private Instant accountExpiry;


    private Collection<? extends GrantedAuthority> authorities;

    public UserPrincipal(UUID externalId, String username, String email, String password, Collection<? extends GrantedAuthority> authorities) {
        this.externalId = externalId;
        this.username = username;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
    }

    public static UserPrincipal create(User user) {
        List<GrantedAuthority> authorities = new ArrayList<>();

        return new UserPrincipal(
                user.getExternalId(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                authorities
        );
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


}
