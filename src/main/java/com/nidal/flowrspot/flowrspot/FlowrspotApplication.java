package com.nidal.flowrspot.flowrspot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class FlowrspotApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlowrspotApplication.class, args);
	}

}
