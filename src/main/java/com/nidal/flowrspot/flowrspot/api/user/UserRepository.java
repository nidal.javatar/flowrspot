package com.nidal.flowrspot.flowrspot.api.user;

import com.nidal.flowrspot.flowrspot.model.user.dao.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsernameOrEmail(String username, String email);

    Optional<User> findByExternalId(UUID externalId);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
