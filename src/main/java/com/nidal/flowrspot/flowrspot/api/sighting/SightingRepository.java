package com.nidal.flowrspot.flowrspot.api.sighting;

import com.nidal.flowrspot.flowrspot.model.sighting.dao.Sighting;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SightingRepository extends JpaRepository<Sighting, Long> {

    List<Sighting> findAllByQuote(String quote);

    List<Sighting> findAllByFlowerExternalId(UUID flowerExternalId);

    Optional<Sighting> findByExternalId(UUID externalId);

    Sighting findByExternalIdAndUserExternalId(UUID externalId, UUID userExternalId);

}
