package com.nidal.flowrspot.flowrspot.api.flower;

import com.nidal.flowrspot.flowrspot.model.flower.dao.Flower;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface FlowerRepository extends JpaRepository<Flower, Long> {

    Boolean existsByExternalId(UUID externalId);

    Optional<Flower> findByExternalId(UUID externalId);

    List<Flower> findAllByOrderByCreatedOnAsc();

}
