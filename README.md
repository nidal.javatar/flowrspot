# FlowrSpot

Clone project on gitlab. Open your IDE (IntelliJ). Import as existing maven project

If you don't have lombok installed, please follow the instructions described in this [article](https://www.baeldung.com/lombok-ide).

*MySql

Run MySql server and create schema flowrspot.

*Run application

Run mvn clean install, then run application like SpringBootApp



